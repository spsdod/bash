#!/bin/bash


# Comments #####################################################################
# Name: Replace Lines
# About: Replaces contents of certain lines in a file using sed.
# Author: spsdod
# Created: February 21, 2017
# Updated: February 27, 2017
# Version: 1.0


# Functions ####################################################################
main()
{
  # Declare and define constants and variables.
  
  # File that has lines you want to replace.
  declare file="file.json"
  declare choice

  # variable used for iterating indices in array_line_numbers  
  declare -i index
  
  # Put the numbers of the lines whose content you want to replace in array.
  declare -a array_line_numbers=( 1 3 5 7 9 11 )
  
  # Variable used for iterating the indices in array_new_strings.
  declare -i strindex=0
  
  # Since my test $file to update was a JSON, I enclosed the variables with
  # single quotes ' ' at the start and end. Quote-wise, adjust the variable
  # values as needed.
  declare -a array_new_strings
  array_new_strings[0]='1ST STRING TO PUT ON FIRST LINE NUMBER'
  array_new_strings[1]='2ND STRING TO PUT ON NEXT LINE NUMBER'
  array_new_strings[2]='3RD STRING TO PUT ON NEXT LINE NUMBER'
  array_new_strings[3]='4TH STRING TO PUT ON NEXT LINE NUMBER'
  array_new_strings[4]='5TH STRING TO PUT ON NEXT LINE NUMBER'
  array_new_strings[5]='6TH STRING TO PUT ON NEXT LINE NUMBER'
  
  # Find $file in $PWD.
  printf "\nSearching present working directory for file\n\n${file}\n\n"
  sleep 1
  if [ ! -f ${file} ]; then
    printf "Error: cannot find file ${file} in PWD\n\n"
    sleep 2
    printf "Please ensure file ${file} exists in the PWD\n\n"
    printf "Finished\n\nBye\n\n"
    exit 1
  fi

  # Make backup of $file and name it $file.old.
  printf "Found file ${file}\n\n"
  sleep 1
  printf "Creating backup of ${file} named ${file}.old\n\n\n"
  sleep 1
  cp ${file} ${file}.old
  
  # Explain what script is about to do.
  printf "This script (${0}) will replace the contents of line numbers\n"
  for line_number in ${array_line_numbers[*]}; do
    printf "${line_number} "
  done
  printf "\nwith the contents of the indices in this script\'s array_new_strings.\n\n"
  
  # Menu
  while true; do
    printf "\nPress <R> followed by <Enter> to replace the lines\' contents\n"
    printf "or\n"
    printf "Press <Q> followed by <Enter> to quit (and possibly edit the script)\n\n"
    read -r -p "===> " choice
    case ${choice} in
      r|R)
        printf "\n"
        break
      ;;
      q|Q|quit|Quit|QUIT)
        printf "\nProbably a good choice.\n"
        printf "\nBye\n\n"
        exit 0
      ;;
      *)
        printf "\nError: invalid menu option.\n\n"
        sleep 2       
        clear
        continue
      ;;
    esac
  done

  # Heart of script.
  for (( index=0; index < ${#array_line_numbers[*]}; index++ )); do
    printf "Moving to line number ${array_line_numbers[${index}]}\n"
    printf "Replacing contents of line number ${array_line_numbers[${index}]} "
    printf "with ${array_new_strings[${strindex}]}\n" 
    sed -i "${array_line_numbers[${index}]}s/.*/${array_new_strings[${strindex}]}/" ${file} 
    (( strindex++ ))
    sleep 1
  done # End for-loop.

  printf "\nFinished\n\nBye\n\n"
  exit 0
}

main "$@"