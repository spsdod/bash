#!/bin/bash


# Comments ######################################################################
# Name:
#   MS DNS Update
#
# Author:
#   spsdod
#
# About:
#   Dynamically update the DNS 'A record' and the DNS 'PTR record' of a Linux
#   host in Microsoft DNS.
#
# Tested:
#   Bash 4.3.11, BIND Utilities 9.9.4, CentOS 7, IPv4, Microsoft DNS 6.3.9600.
#
# Created:
#   May 15, 2017
#
# Updated:
#   August 3, 2017
#
# Notes:
#   1. Useful to run at boot with systemd.
#   2. Useful to add as a cron job.
#   3. Tested with Microsft DNS zones set to allow nonsecure and secure dynamic
#      updates. Later, this script will be set to work with only secure updates.


# Main ##########################################################################
# Declare and define constants and variables.
declare dir="/usr/bin/"
declare nsupdate_path="${dir}nsupdate"
declare ns1
declare ip
declare ip_rev
declare -i distro
declare zone="dmz.int"


# Exit if OS not Linux (e.g. AIX, Darwin, OpenBSD, SunOS, etc.)
if [ $(uname) != "Linux" ]; then
  printf "\nError: this UNIX or Unix-like OS is not Linux.\n"
  printf "\nBye\n\n"
  exit 1
fi

# Assign a value of zero to the integer variable 'distro' to indicate this
# system is running a Linux distribution of some type.
distro=0

# Determine if Linux distribution is Debian-based or Fedora-based.
# Debian-based distribution equals 1.
# Fedora-based distribution equals 2.
grep -e /etc/*release -e [dD]ebian -e [uU]buntu &> /dev/null
[ $? -eq 0 ] && distro=1

grep -e /etc/*release -e [cC]ent[oO][sS] -e [fF]edora -e rhel &> /dev/null
[ $? -eq 0 ] && distro=2

# Exit script if nsupdate binary is not installed on this host.
if [ ! -e ${nsupdate_path} ]; then
  printf "\nError: cannot locate nsupdate in directory ${dir}\n\n"
  case ${distro} in
    0)
      printf "Unable to identify this distribution of Linux.\n"
      printf "Please install the package which contains nsupdate.\n"
    ;;

    1)
      printf "Please install the dnsutils package, which contains nsupdate.\n"
      printf "\nExample:\napt-get -y install dnsutils\n"
    ;;

    2)
      printf "Please install the bind-utils package, which contains nsupdate.\n"
      printf "\nExample:\nyum -y install bind-utils\n"
    ;;

    *)
      printf "Wow! Not sure how that happened. An integer value outside the\n"
      printf "scope of the case\nstatement currently running in this script\n"
      printf "was assigned to the variable \'distro\'\nGuess we better exit.\n"
  esac
  printf "\nBye\n\n"
  exit 2
fi

# Exit if zone variable is null.
if [ -z ${zone} ]; then
  printf "\nError: no value assigned to the zone variable in this script.\n"
  printf "The zone variable and its value should look something like this:\n\n"
  printf "declare zone=\"ad.domain.com\"\n"
  printf "\nBye\n\n"
  exit 3
fi


# Exit if IPv6, or first DNS nameserver is IPv6.
grep ":" /etc/resolv.conf &> /dev/null
if [ $? -eq 0 ]; then
  printf "\nError: this Linux host appears to be using IPv6. This script is"
  printf " not configured\nfor IPv6.\n\nBye\n\n"
  exit 4
fi


# Get the IP address of the first DNS nameserver listed in /etc/resolv.conf.
ns1=$(cat /etc/resolv.conf | grep -i nameserver | head -n1 | cut -d ' ' -f2)

# Get the IP address of this host.
ip=$(hostname -I)

# Get the hostname of this host.
host=$(hostname)

# Update the DNS nameserver A-record for this host.
nsupdate << EOF
server ${ns1}
update delete ${host}.${zone}. A
update add ${host}.${zone}. 86400 A ${ip}
show
send
EOF


# Use Bash substring replacement to replace all "." chars with " " chars.
octets=${ip//./ }

# Set value of octets as positional parameters (arguments), so Bash will set
# each space separated item in octets as a $1 $2 $3 $4...
set ${octets}

# Reverse octet values and assign to ip_rev.
ip_rev=${4}.${3}.${2}.${1}

# Update the DNS nameserver PTR record for this host.
nsupdate << EOF
server ${ns1}
update add ${ip_rev}.in-addr.arpa 86400 PTR ${host}.${zone}.
show
send
EOF

exit 0