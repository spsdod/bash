#!/bin/bash


# Comments #####################################################################
# Name: Ports LISTENING
# About: Use ss to check if an array of TCP port numbers are LISTENING.
# Author: spsdod 
# Version: 1.0
# Tested: Bash 4.3.11, CentOS 7
# Created: April 26, 2017
# Updated: April 27, 2017
# Notes:
#   1. Written for Orion SAM as a custom script monitor.


# Functions ####################################################################
show_banner(){
  local char="="
  local -i count

  printf "\n"
  for (( count=0; count<=80; count++ )); do 
    printf "${char}"
  done
  printf "\n                                   Welcome to\n"
  printf "                                 Ports LISTENING\n"
  for (( count=0; count<=80; count++ )); do
    printf "${char}"
  done
  printf "\n\n"
  return 0
}


# Call function show_banner.
#show_banner

# Declare and define variables.
declare -a array_ports_to_check=(10023 10024)
declare port
declare -a array_ports_found_closed
declare -i index=0
declare ports_listening
declare ports_closed


# Use ss to see if TCP ports in array_ports_to_check are LISTENING. If ports are
# CLOSED or FILTERED, add each CLOSED or FILTERED port number as an index to
# array_ports_found_closed.
for port in ${array_ports_to_check[*]}; do
  ss -atln | grep -o $port &> /dev/null
  if [ $? -eq 1 ]; then
    array_ports_found_closed[$index]=$port
    (( index++ ))
  fi
done

# Re-define index as an integer with a value of 0.
declare -i index=0

# If all ports in array_ports_to_check were LISTENING then concatenate each of
# the LISTENING port numbers to ports_listening.
if [ ${#array_ports_found_closed[*]} -lt 1 ]; then
  while [ $index -lt ${#array_ports_to_check[*]} ]; do
    ports_listening="$ports_listening ${array_ports_to_check[$index]}"
    (( index++ ))
  done
  # ports_listening was null, it's first defined by the while-loop as itself
  # followed by a space; meaning the first string in ports_listening is a space
  # then each port number LISTENING. Use xargs to trim the space.
  ports_listening=$(printf "$ports_listening" | xargs)
  echo "Statistic: 0"
  echo "Message: Ports LISTENING $ports_listening"
  exit 0
fi


# If any ports in array_ports_to_check are CLOSED or FILTERED
while [ $index -le ${#array_ports_found_closed[*]} ]; do
  ports_closed="$ports_closed ${array_ports_found_closed[$index]}"
  (( index++ ))
done
ports_closed=$(printf "$ports_closed" | xargs)
echo "Statistic: 3"
echo "Message: Ports CLOSED or FILTERED $ports_closed"
exit 0
