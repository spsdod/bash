#!/bin/bash


# Comments #####################################################################
# Name: Process Count
# About: Confirm a process is running and its process count.
# Author: spsdod 
# Version: 1.0
# Tested: Bash 4.3.11, CentOS 7
# Created: April 27, 2017
# Updated: April 27, 2017
# Notes:
#   1. Written for Orion SAM as a custom script monitor.


# Name of process to get the running count of.
declare proc_name=${1}

# The minimum count of proc_name processes that should be running.
declare -i proc_min=2

# The count of proc_name processes running.
declare -i proc_count=$(ps -e | grep -c ${proc_name} | grep -v grep)


if (( proc_count < proc_min )); then
  echo "Statistic: 3"
  echo "Message: Process ${proc_name} is running less than ${proc_count} times."
elif (( proc_count > proc_min )); then
  echo "Statistic: 2"
  echo "Message: Process ${proc_name} is running more than ${proc_count} times."
else
  echo "Statistic: 0"
  echo "Message: Process ${proc_name} is running ${proc_count} times."
fi
exit 0
