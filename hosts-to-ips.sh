#!/bin/bash


# Comments #####################################################################
# Name: Hosts to IPs
# About: Takes list of hostnames from file and returns IPs to other file
# Author: spsdod
# Created: October 21, 2015 (Back to the Future Day)
# Tested: Bash 4.2.46, CentOS 7
# Version: 1.0
# Updated: October 21, 2015 (Back to the Future Day)


# Functions ####################################################################
# Clobber old output file.
clobber_output_file()
{
  [[ -f ${out} ]] && cat /dev/null > ${out}
}


# Show banner with title and description of script.
show_banner()
{
  local char='='
  local -i count

  printf "\n"
  for (( count=0; count<=80; count++ )); do
    printf "${char}"
  done
  printf "\n                                   Welcome to\n"
  printf "                                  Hosts to IPs\n"
  for (( count=0; count<=80; count++ )); do
    printf "${char}"
  done
  printf "\n\n"
  return 0
}

# Handle errors.
handle_error()
{
  case $error in
    1)
      printf "\n\nNo input file of hostnames named ${in} exists"
      printf " to read in\nthe present working directory.\n"
      printf "\nBye\n\n"
      exit 1
    ;;
  esac
}


# Main #########################################################################
declare out="ips.txt"
declare in="hosts.txt"
declare -i error=0
declare line
declare ip

# Call function show_banner.
show_banner

# If file of IPs from last run exists then delete all data in that file.
clobber_output_file $out

printf "\nLooking in present working directory for a file named ${in}\n"
printf "(${in} should have a hostname on each line)\n\n"
sleep 3
# Or if it is FALSE that an input file of hostnames exists
[[ -f ${in} ]] || error=1; handle_error ${error}
printf "Found file ${in} in present working directory.\n"
sleep 2
printf "\nGetting IPs...\n"
sleep 2

for line in $(cat ${in}); do
  # Ping host once, take third column of first line of ping's output, remove 1st
  # char and last char of what is left (should be the parenthesis()), leaving only
  # the IP address.
  ip=$(ping -c 1 ${line} | head -n1 | awk '{ print $3 }' | sed 's:^.\(.*\).$:\1:')
  printf "Getting IP address ${ip} for hostname ${line}\n"
  printf "${ip}\n" >> ${out}
done
printf "\nDone\n"
printf "\nMade file ${out} with the IP address of each hostname from ${in}\n"

printf "\nBye\n\n"

exit 0