#!/bin/bash


# Comments #####################################################################
# Name: File System Capacity
# Author: spsdod
# Created: October 15th, 2015
# Description: Monitors file system capacity in percent and performs action
# Tested: Bash 4.1.2, CentOS 7
# Version: 1.0
# Updated: March 11th, 2016
# Notes:
#  1. File system must be defined as variable fs (below), or passed
#	    as the first argument when executing the script.
#
#  2. Written for Orion SAM as a custom script monitor.


# Functions ####################################################################
# Ensures the file system is mounted
is_fs_mounted()
{
  df -h | grep -w ${fs} &> /dev/null
  if [ $? -eq 1 ]; then
    echo "Statistic: 3"
    echo "Message: File system ${fs} not mounted"
    exit 0
  fi
}


# Main #########################################################################
# Declare and define local constants and variables
declare -ir CRIT=90
declare -ir WARN=85
fs=${1}
capacity=
percent=

# Call function is_fs_mounted to confirm file system is mounted.
is_fs_mounted

# Define capacity as the last line (tail), reversed (rev) of reported 
# disk-file system info (df) in human readable format (-h).
capacity=$(df -h ${fs} | tail -1 | rev)

# Set value of capacity as positional parameters (arguments). Each
# space-separated item in capacity will be $1 $2 $3 $4... 
set ${capacity}

# Define percent as 2nd parameter set from capacity. This results in 
# value of percent being a % symbol with a reversed number: %16 vs %61.
percent=$(echo ${2})

# With parameter expansion-substring removal, re-define percent as removal
# of shortest pattern from start of percent (#*) that matches a % symbol.
percent=${percent#*%}

# Re-define percent with its value reversed: 61 vs 16.
percent=$(echo ${percent} | rev)

# Re-define variant type of percent from string to integer so we can do
# math on it a few lines below this.
declare -i percent=${percent}

# Use (( )) construct for arithmetic to see if percent is greater
# than or equal to CRIT. No sigil ($) needed when using (( )).
if (( percent >= CRIT )); then
  echo "Statistic: 3"
  echo "Message: File system ${fs} is at ${percent}% capacity"
  exit 0
fi

if (( percent >= WARN )); then
  echo "Statistic: 2"
  echo "Message: File system ${fs} is at ${percent}% capacity"
  exit 0
fi

echo "Statistic: 0"
echo "Message: File system ${fs} is at ${percent}% capacity"
exit 0