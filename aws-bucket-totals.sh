#!/bin/bash


# Comments #####################################################################
# Name: AWS Bucket Totals
# About: Gets object count and total size of AWS S3 Buckets
# Author: spsdod
# Created: February 14, 2017 (Valentine's Day)
# Updated: February 14, 2017 (Valentine's Day)
# Version: 1.0 


# Functions ####################################################################
show_banner(){
  local char="="
  local -i count

  printf "\n"
  for (( count=0; count<=80; count++ )); do 
    printf "${char}"
  done
  printf "\n                                   Welcome to\n"
  printf "                                AWS Bucket Totals\n"
  for (( count=0; count<=80; count++ )); do
    printf "${char}"
  done
  printf "\n\n"
  return 0
}


read_me(){
  printf "This script assumes that you have:\n"
  printf "_Installed the AWSCLI\n"
  printf "_Input your Access Key ID\n"
  printf "_Input your Secret Access Key\n"
  printf "_A line delimited list of the S3 Buckets named $s3buckets\n"
  printf "_A line delimited list of the S3 Regions named $s3regions matching $s3bbuckets\n"
  sleep 2 
  return 0
}


# Didn't call this function, may implement later.
match_buckets_regions(){
  local -i number_of_buckets
  local -i number_of_regions
  
  sleep 2
  number_of_buckets=${#array_bucket_names[*]}
  printf "Array of bucket names has $num_of_bkts Buckets\n\n"

  number_of_regions=${#array_bucket_regions[*]}
  printf "Array of bucket regions has $num_of_regions regions\n\n"
  
  if [ $number_of_buckets -ne $number_of_regions ]; then
    printf "Error:  all the buckets do not have matching regions\n"
    exit 1
  fi
  return 0
}


main(){
  # Declare and define variables
  declare line
  
  declare s3buckets=s3buckets.txt
  declare -a array_bucket_names
  declare -i item_number=0
   
  declare s3regions=s3regions.txt
  declare -a array_bucket_regions
 
  declare bucket
  declare region
  declare choice
  
  declare -i item_number  
  declare start_time
  declare index
  declare pid
  declare -a array_pids

  declare -i procs_finished
  declare pid_status
  declare -i procs_running
  
  declare aws_file
  declare totals_file=aws-bucket-totals.txt
  declare finish_time

  # Call function show_banner.
  show_banner

  # Call function read_me, pass it variables $s3buckets and $s3regions.
  read_me $s3buckets $s3regions
  
  # Check if $s3buckets exists in pwd.
  printf "\nLooking for file $s3buckets in the present working directory...\n"
  printf "($s3buckets should be a line-delimited list of S3 Buckets)\n"
  sleep 4
  if [ ! -f $s3buckets ]; then
    printf "\nError: $s3buckets not found in present working directory.\n\nBye\n\n"
    exit 1
  else
    printf "Found $s3buckets\n"
    sleep 1
  fi

  # Check if $s3regions exists in pwd.
  printf "\nLooking for file $s3regions in the present working directory...\n"
  printf "($s3regions should be a line-delimited list of S3 Regions)\n"
  sleep 4
  if [ ! -f $s3regions ]; then
    printf "\nError: $s3regions not found in present working directory.\n\nBye\n\n"
    exit 1   
  fi

  # Read bucket names from $s3buckets into $array_bucket_names.
  printf "\nReading S3 Bucket names. Stand by... \n"
  sleep 2
  while read line; do
  array_bucket_names[$item_number]=$line
    (( item_number++ ))
  done < $s3buckets
  echo ${#array_bucket_names[*]}

  
  # Re-value integer variable $item_number as 0.
  declare -i item_number=0
  # Read S3 Bucket Regions from $s3regions into $array_bucket_regions. 
  printf "\nReading corresponding S3 Bucket Regions. Stand by... \n"
  while read line; do
    array_bucket_regions[$item_number]=$line
    (( item_number++ ))
  done < $s3regions
  echo ${#array_bucket_regions[*]}

  # Print list of buckets to get totals of.
  while true; do
    printf " ===S3 Buckets===\n"
    for (( index = 0; index < ${#array_bucket_names[*]}; index++ )); do
      printf "${array_bucket_names[$index]} ${array_bucket_regions[$index]}\n"
    done
    printf " ===S3 Buckets===\n"
    printf "\nPress <T> followed by <Enter> to get the totals for the listed S3 Buckets\n"
    printf "or\n"
    printf "Press <Q> followed by <Enter> to quit\n\n"
    read -r -p "===> " choice
    case $choice in
      t|T)
        printf "\n"
        break
      ;;
      q|Q|quit|Quit|QUIT)
        printf "\nProbably a good choice.\n"
        printf "\nBye\n\n"
        exit 0
      ;;
      *)
        printf "\nError: invalid menu option.\n\n"
        sleep 2       
        clear
        show_banner
        continue
      ;;
    esac
  done

  # Process each bucket and its region (heart of script).
  declare -i item_number=0
  declare start_time=$(date +%A\ %B\ %d,\ %Y\ @\ %H:%M:%S)
  printf "Started $start_time\n\n"
  # For-loop iterates buckets and regions through command aws s3...
  # Run aws command for each bucket in subshell in background.
  for (( index = 0; index < ${#array_bucket_names[*]}; index++ )); do
    ( aws s3 ls s3://${array_bucket_names[$index]}  --human-readable --summarize --region "${array_bucket_regions[$index]}" | grep Total > ${array_bucket_names[$index]}.aws ) &
    pid=$!
    disown
    printf "pid $pid summarizing totals for bucket ${array_bucket_names[$index]}\n"
    array_pids[$item_number]=$pid
    (( item_number++ ))
  done

  # Until done, update terminal with bucket totals still being calculated.
  declare pid
  declare -i procs_finished=0
  printf "\nStatus of processes:\n"
  
  while [ $procs_finished -lt ${#array_pids[*]} ]; do
    declare -i procs_running=${#array_pids[*]}
    
    for pid in ${array_pids[*]}; do
      ps $pid > /dev/null
      pid_status=$?
      if [ $pid_status -eq 1 ]; then
        (( procs_finished++ ))
        (( procs_running-- ))
      fi
    done # end for-loop.
    
    # This printf line works well.
    printf "\r$procs_running of ${#array_pids[*]} processes still running. \b"
    if [ $procs_finished -eq ${#array_pids[*]} ]; then
      break # Break the while-loop if done processing
    else
      procs_finished=0
    fi
  done # End while-loop.

  # Write object count and size of each bucket to one file.
  for aws_file in *.aws; do
    printf "$aws_file\n" >> $totals_file
    head $aws_file >> $totals_file
    printf "\n" >> $totals_file
  done
  sed -i 's/.aws//' $totals_file
  # Delete temp file (given .aws extension) for each bucket.
  rm -rf *.aws

  declare finish_time=$(date +%A\ %B\ %d,\ %Y\ @\ %H:%M:%S)
  printf "\n\nFinished $finish_time\n\nBye\n\n"
  exit 0
}


main "$@"
